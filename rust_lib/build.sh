#!/bin/sh
JNI_LIBS=../app/src/main/jniLibs

cargo build --target aarch64-linux-android --release

rm -rf $JNI_LIBS
mkdir $JNI_LIBS
mkdir $JNI_LIBS/arm64-v8a


cp target/aarch64-linux-android/release/librust_lib.so $JNI_LIBS/arm64-v8a/librust_lib.so
