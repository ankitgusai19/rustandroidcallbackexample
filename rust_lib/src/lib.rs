//Run build.sh to create so file
//cargo build --target aarch64-linux-android --release
#![cfg(target_os = "android")]
#![allow(non_snake_case)]

//mod mci;

use jni::JNIEnv;
use jni::objects::{JClass, JObject, JString, JValue, GlobalRef};
use jni::sys::{jstring};
use android_logger::Config;
use log::Level;
use std::time::Duration;
use std::thread;
use std::sync::mpsc;
use futures::select;
use futures::executor::block_on;


/*#[no_mangle]
pub unsafe extern fn Java_com_ankit_rustjavaapplication_RustInterface_helloRust(env: JNIEnv, _: JObject, javaInterface: JObject, j_recipient: JString) -> jstring {
    let input: String = env
        .get_string(j_recipient)
        .expect("Couldn't get java string!")
        .into();

    // Then we have to create a new java string to return. Again, more info
    // in the `strings` module.
    let output = env
        .new_string(format!("Hello, {}!", input))
        .expect("Couldn't create java string!");

    let arg = JValue::Object(output.into());
    //Lcom/ankit/rustjavaapplication/JavaInterface;
    env
        .call_method(javaInterface, "helloJava", "(Ljava/lang/String;)V", &[arg])
        .unwrap();


    // Finally, extract the raw pointer to return.
    output.into_inner()
}
*/
#[no_mangle]
pub unsafe extern fn Java_com_ankit_rustjavaapplication_RustInterface_initRust(env: JNIEnv, _: JObject) {
    android_logger::init_once(
        Config::default()
            .with_min_level(Level::Trace)
            .with_tag("Rust"),
    );
    log_panics::init();
    log::info!("Logging initialised from Rust");

    //RANDOM_CLASS_REF = Some(env.new_global_ref(randomClass).unwrap());

    let (vpnTx, vpnRx) = mpsc::channel();
    let (uiSwitchTx, uiSwitchRx) = mpsc::channel();

    #[no_mangle]
    pub unsafe extern fn Java_com_ankit_rustjavaapplication_RustInterface_start(env: JNIEnv, _: JObject, randomClass: JObject) {
        let jvm = env.get_java_vm().unwrap();

        //simulate running vpn
        let _ = thread::spawn(move || {
            // Use the `JavaVM` interface to attach a `JNIEnv` to the current thread.
            let env = jvm.attach_current_thread().unwrap();
            let mut progress = 1;
            loop {
                progress = progress + 1;

                let arg = JValue::Int(progress.into());

                // env
                //     .call_method(randomClass, "anotherVpnUtilForRust", "(Ljava/lang/Integer;)V", &[arg])
                //     .unwrap();

                thread::sleep(Duration::from_millis(1000));
            }

            // The current thread is detached automatically when `env` goes out of scope.
        });
    }
    #[no_mangle]
    pub unsafe extern fn Java_com_ankit_rustjavaapplication_RustInterface_terminate(env: JNIEnv, _: JObject) {
        //uiSwitchTx.send(()).unwrap();
    }

    block_on(async {
        select! {
    _ = vpnRx.recv() => println!("vpn Exited"),
    _ = uiSwitchRx.recv() => {
        println!("user killed vpn");
    }
}
    });
}


/*
#[no_mangle]
pub unsafe extern fn Java_com_ankit_rustjavaapplication_RustInterface_callRandomClassRust(env: JNIEnv, _: JObject) {
    env
        .call_method(RANDOM_CLASS_REF.as_ref().unwrap().as_obj(), "randomMethod", "()V", &[])
        .unwrap();
}*/








