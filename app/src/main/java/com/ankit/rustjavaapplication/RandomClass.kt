package com.ankit.rustjavaapplication

import android.util.Log
import kotlin.math.log

class RandomClass(private val randomValue: Int) {
    init {
        Log.d("TAG", "object ${this}, of RandomClass created with value : $randomValue")
    }

    //print current object reference
    fun randomMethod(){
        Log.d("TAG", "object ${this}, value: $randomValue")
    }
}