package com.ankit.rustjavaapplication

class RustInterface {
    //external fun helloRust(jObject: JavaInterface, to: String): String
    //external fun helloInner(): String
    external fun initRust(vpnServiceInterface: VpnServiceInterface)
    //external fun callRandomClassRust()
    //external fun anotherRustFunction()
    external fun start()
    external fun terminate()
}