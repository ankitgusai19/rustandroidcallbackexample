package com.ankit.rustjavaapplication

import android.util.Log
import kotlin.random.Random

class VpnServiceInterface() {

    public fun protect(): Int {
        return Random(1000).nextInt()
    }

    public fun anotherVpnUtilForRust(int: Int) {
        Log.d("TAG", "anotherVpnUtilForRust:$int ")
    }
}