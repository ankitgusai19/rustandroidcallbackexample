package com.ankit.rustjavaapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button

class MainActivity : AppCompatActivity() {

    //methods defined on rust side
    val rustInterface = RustInterface()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Laod lib
        System.loadLibrary("rust_lib")


        //test methods
        /*val javaInterface = JavaInterface()
        val rustRes = rustInterface.helloRust(javaInterface, "Ankit");*/

        //init with random class object (reference of RandomClass is hold at rust side)

        Thread {
            rustInterface.initRust(VpnServiceInterface())
        }.start()


        findViewById<Button>(R.id.button).setOnClickListener {
            rustInterface.start()
        }

        findViewById<Button>(R.id.button2).setOnClickListener {
            rustInterface.terminate()
        }
    }

}
